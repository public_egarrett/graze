defmodule GrazeWeb.RecipeView do
  use GrazeWeb, :view
  alias GrazeWeb.RecipeView

  def render("index.json", %{recipes: recipes}) do
    %{data: render_many(recipes, RecipeView, "recipe.json")}
  end

  def render("show.json", %{recipe: recipe}) do
    %{data: render_one(recipe, RecipeView, "recipe.json")}
  end

  def render("recipe.json", %{recipe: recipe}) do
    %{id: recipe.id,
      title: recipe.title,
      body: recipe.body,
      source: recipe.source,
      image: recipe.image,
      image_type: recipe.image_type}
  end
end
