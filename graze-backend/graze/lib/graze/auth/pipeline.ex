defmodule GrazeWeb.Auth.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :graze,
    module: GrazeWeb.Auth.Guardian,
    error_handler: GrazeWeb.Auth.ErrorHandler

  plug Guardian.Plug.VerifyHeader
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
