defmodule Graze.Recipes.Recipe do
  use Ecto.Schema
  import Ecto.Changeset

  schema "recipes" do
    field :body, :string
    field :image, :binary
    field :image_type, :string
    field :source, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(recipe, attrs) do
    recipe
    |> cast(attrs, [:title, :body, :source, :image, :image_type])
    |> validate_required([:title, :body, :source])
  end
end
