defmodule Graze.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Graze.Repo,
      # Start the Telemetry supervisor
      GrazeWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Graze.PubSub},
      # Start the Endpoint (http/https)
      GrazeWeb.Endpoint,
      # Start a worker by calling: Graze.Worker.start_link(arg)
      # {Graze.Worker, arg}
      # Guardian Token Sweeper
      {Guardian.DB.Token.SweeperServer, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Graze.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    GrazeWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
