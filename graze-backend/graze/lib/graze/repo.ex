defmodule Graze.Repo do
  use Ecto.Repo,
    otp_app: :graze,
    adapter: Ecto.Adapters.Postgres
end
