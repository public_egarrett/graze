defmodule Graze.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :firstname, :string
    field :lastname, :string
    field :hash, :string
    field :uname, :string
    field :pw, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :pw, :firstname, :lastname, :uname])
    |> validate_required([:email, :pw, :firstname, :lastname, :uname])
    |> validate_format(:email, ~r/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)
    |> validate_length(:pw, min: 6)
    |> unique_constraint(:email)
    |> put_hashed_password
  end

  defp put_hashed_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{pw: pw}}
        ->
        put_change(changeset, :hash, Comeonin.Bcrypt.hashpwsalt(pw))
        _ ->
          changeset
    end
  end
end
