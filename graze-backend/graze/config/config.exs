# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :graze,
  ecto_repos: [Graze.Repo]

# Configures the endpoint
config :graze, GrazeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "BPxbHZOO+gL0mME0qGrxBBsfEuBId0zYJMg6xx0XvpUWzHqBunab4su3YrIlSpHy",
  render_errors: [view: GrazeWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Graze.PubSub,
  live_view: [signing_salt: "lX8QtOmW"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :graze, GrazeWeb.Auth.Guardian,
  issuer: "graze",
  secret_key: ""
