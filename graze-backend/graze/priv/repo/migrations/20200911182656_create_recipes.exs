defmodule Graze.Repo.Migrations.CreateRecipes do
  use Ecto.Migration

  def change do
    create table(:recipes) do
      add :title, :string
      add :body, :string
      add :source, :string
      add :image, :binary
      add :image_type, :string

      timestamps()
    end

  end
end
