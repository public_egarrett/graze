defmodule Graze.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :hash, :string
      add :firstname, :string
      add :lastname, :string
      add :uname, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
