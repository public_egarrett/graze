# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Graze.Repo.insert!(%Graze.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Graze.Repo
alias Graze.Recipes.Recipe

Repo.insert! %Recipe{title: "Test Recipe 1", body: "idk throw some stuff in a pot and cook it, bro", source: "http://www.google.com"}
